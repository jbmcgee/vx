#define VK_EXPORTS                                              \
VK_EXPORT(vkGetInstanceProcAddr)

#define VK_GLOBAL_IMPORTS                                       \
VK_GLOBAL_IMPORT(vkCreateInstance)                              \
VK_GLOBAL_IMPORT(vkEnumerateInstanceExtensionProperties)        \
VK_GLOBAL_IMPORT(vkEnumerateInstanceLayerProperties)       

#if defined(VX_WIN32)
#define VK_INSTANCE_IMPORT_EXTENSIONS                           \
VK_INSTANCE_IMPORT(vkCreateWin32SurfaceKHR)
#elif defined(VX_OSX)
#define VK_INSTANCE_IMPORT_EXTENSIONS                           \
VK_INSTANCE_IMPORT(vkCreateMacOSSurfaceMVK) 
#endif

#define VK_INSTANCE_IMPORTS                                     \
VK_INSTANCE_IMPORT(vkDestroyInstance)                           \
VK_INSTANCE_IMPORT(vkEnumeratePhysicalDevices)                  \
VK_INSTANCE_IMPORT(vkGetPhysicalDeviceProperties)               \
VK_INSTANCE_IMPORT(vkGetPhysicalDeviceFeatures)                 \
VK_INSTANCE_IMPORT(vkGetPhysicalDeviceQueueFamilyProperties)    \
VK_INSTANCE_IMPORT(vkCreateDevice)                              \
VK_INSTANCE_IMPORT(vkGetDeviceProcAddr)                         \
VK_INSTANCE_IMPORT(vkEnumerateDeviceExtensionProperties)        \
VK_INSTANCE_IMPORT(vkDestroySurfaceKHR)                         \
VK_INSTANCE_IMPORT(vkGetPhysicalDeviceSurfaceSupportKHR)        \
VK_INSTANCE_IMPORT(vkGetPhysicalDeviceSurfaceCapabilitiesKHR)   \
VK_INSTANCE_IMPORT(vkGetPhysicalDeviceSurfaceFormatsKHR)        \
VK_INSTANCE_IMPORT(vkGetPhysicalDeviceSurfacePresentModesKHR)   \
VK_INSTANCE_IMPORT(vkGetPhysicalDeviceSurfacePresentModesKHR)   \
VK_INSTANCE_IMPORT(vkGetPhysicalDeviceSurfacePresentModesKHR)   \
VK_INSTANCE_IMPORT(vkGetPhysicalDeviceMemoryProperties)         \
VK_INSTANCE_IMPORT_EXTENSIONS


#define VK_DEVICE_IMPORTS                                       \
VK_DEVICE_IMPORTS(vkGetDeviceQueue)                             \
VK_DEVICE_IMPORTS(vkDestroyDevice)                              \
VK_DEVICE_IMPORTS(vkDeviceWaitIdle)                             \
VK_DEVICE_IMPORTS(vkCreateSwapchainKHR)                         \
VK_DEVICE_IMPORTS(vkDestroySwapchainKHR)                        \
VK_DEVICE_IMPORTS(vkGetSwapchainImagesKHR)                      \
VK_DEVICE_IMPORTS(vkAcquireNextImageKHR)                        \
VK_DEVICE_IMPORTS(vkQueuePresentKHR)                            \
VK_DEVICE_IMPORTS(vkQueueSubmit)                                \
VK_DEVICE_IMPORTS(vkCreateSemaphore)                            \
VK_DEVICE_IMPORTS(vkDestroySemaphore)                           \
VK_DEVICE_IMPORTS(vkCreateCommandPool)                          \
VK_DEVICE_IMPORTS(vkAllocateCommandBuffers)                     \
VK_DEVICE_IMPORTS(vkBeginCommandBuffer)                         \
VK_DEVICE_IMPORTS(vkCmdPipelineBarrier)                         \
VK_DEVICE_IMPORTS(vkCmdClearColorImage)                         \
VK_DEVICE_IMPORTS(vkEndCommandBuffer)                           \
VK_DEVICE_IMPORTS(vkDestroyCommandPool)                         \
VK_DEVICE_IMPORTS(vkFreeCommandBuffers)                         \
VK_DEVICE_IMPORTS(vkCreateFence)                                \
VK_DEVICE_IMPORTS(vkWaitForFences)                              \
VK_DEVICE_IMPORTS(vkResetFences)                                \
VK_DEVICE_IMPORTS(vkDestroyFence)                               \
VK_DEVICE_IMPORTS(vkCreateImageView)                            \
VK_DEVICE_IMPORTS(vkCreateFramebuffer)                          \
VK_DEVICE_IMPORTS(vkCreatePipelineLayout)                       \
VK_DEVICE_IMPORTS(vkCreateRenderPass)                           \
VK_DEVICE_IMPORTS(vkCreateShaderModule)                         \
VK_DEVICE_IMPORTS(vkCreateGraphicsPipelines)                    \
VK_DEVICE_IMPORTS(vkCmdBindPipeline)                            \
VK_DEVICE_IMPORTS(vkCmdDraw)                                    \
VK_DEVICE_IMPORTS(vkCmdBeginRenderPass)                         \
VK_DEVICE_IMPORTS(vkCmdEndRenderPass)                           \
VK_DEVICE_IMPORTS(vkCmdCopyBuffer)                              \
VK_DEVICE_IMPORTS(vkDestroyPipeline)                            \
VK_DEVICE_IMPORTS(vkDestroyRenderPass)                          \
VK_DEVICE_IMPORTS(vkDestroyFramebuffer)                         \
VK_DEVICE_IMPORTS(vkDestroyShaderModule)                        \
VK_DEVICE_IMPORTS(vkDestroyPipelineLayout)                      \
VK_DEVICE_IMPORTS(vkDestroyImageView)                           \
VK_DEVICE_IMPORTS(vkCreateBuffer)                               \
VK_DEVICE_IMPORTS(vkBindBufferMemory)                           \
VK_DEVICE_IMPORTS(vkMapMemory)                                  \
VK_DEVICE_IMPORTS(vkFlushMappedMemoryRanges)                    \
VK_DEVICE_IMPORTS(vkUnmapMemory)                                \
VK_DEVICE_IMPORTS(vkGetBufferMemoryRequirement                  \
VK_DEVICE_IMPORTS(vkAllocateMemory)                             \
VK_DEVICE_IMPORTS(vkCmdSetViewport)                             \
VK_DEVICE_IMPORTS(vkCmdSetScissor)                              \
VK_DEVICE_IMPORTS(vkCmdBindVertexBuffers)                       \
VK_DEVICE_IMPORTS(vkFreeMemory)                                 \
VK_DEVICE_IMPORTS(vkDestroyBuffer)  

#define VK_EXPORT(n, ...) PFN_##n n;
VK_EXPORTS
#undef VK_EXPORT

#define VK_GLOBAL_IMPORT(n, ...) PFN_##n n;
VK_GLOBAL_IMPORTS
#undef VK_GLOBAL_IMPORT

#define VK_INSTANCE_IMPORT(n, ...) PFN_##n n;
VK_INSTANCE_IMPORTS
#undef VK_INSTANCE_IMPORT

#define VK_DEVICE_IMPORTS(n, ...) PFN_##n n;
VK_DEVICE_IMPORTS
#undef VK_DEVICE_IMPORTS