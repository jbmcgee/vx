#include "gfx.h"

void vx_init()
{
    vx_gfx_init();
}

void vx_uninit()
{
    vx_gfx_uninit();
}

void vx_render()
{
    vx_gfx_begin();
    vx_gfx_end();
}

void vx_update()
{

} 