#pragma once
#include "vx.h"

uint32_t vx_gfx_init();
void vx_gfx_uninit();
void vx_gfx_begin();
void vx_gfx_end();
