#if VX_WIN32
#include <windows.h>
#include <stdio.h>
#include "vx.h"

HWND _hwnd;
HINSTANCE _hinstance;

vx_window_t _osWindow;
char _execDir[VX_MAX_PATH];

static void set_exec_path(const wchar_t* path)
{
    wcstombs(_execDir, path, VX_MAX_PATH);
    int i = strlen(_execDir) - 1;
    while(_execDir[i] != '\\') i--;
    _execDir[i] = 0;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
        return 0;

    case WM_PAINT:
        vx_update();
        vx_render();
        return 0;

    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
}

int CALLBACK WinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR     lpCmdLine,
    int       nCmdShow
)
{
 // Parse the command line parameters
    int argc;
    LPWSTR* argv = CommandLineToArgvW(GetCommandLineW(), &argc);
    set_exec_path(argv[0]);
    LocalFree(argv);

    // Initialize the window class.
    WNDCLASSEX windowClass = { 0 };
    windowClass.cbSize = sizeof(WNDCLASSEX);
    windowClass.style = CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = WindowProc;
    windowClass.hInstance = hInstance;
    windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    windowClass.lpszClassName = "VXClass";
    RegisterClassEx(&windowClass);

    RECT windowRect = { 0, 0, 640, 480 };
    AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE);

    // Create the window and store a handle to it.
    _hwnd = CreateWindow(
        windowClass.lpszClassName,
        "VX",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        windowRect.right - windowRect.left,
        windowRect.bottom - windowRect.top,
        NULL,        // We have no parent window.
        NULL,        // We aren't using menus.
        hInstance,
        NULL);
    _hinstance = hInstance;

    _osWindow.handle = _hwnd;
    _osWindow.instance = _hinstance;
    _osWindow.isOpen = 1;
    _osWindow.width = windowRect.right - windowRect.left;
    _osWindow.height = windowRect.bottom - windowRect.top;
    
    // Initialize
    vx_init();

    ShowWindow(_hwnd, nCmdShow);

    // Main sample loop.
    MSG msg = { 0 };
    while (msg.message != WM_QUIT)
    {
        // Process any messages in the queue.
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    // Destroy
    vx_uninit();

    // Return this part of the WM_QUIT message to Windows.
    return (char)(msg.wParam);
}

vx_window_t* vx_os_get_window() {
     return &_osWindow;
}

void* vx_os_open_library (const char* libname)
{
    return (void*)LoadLibrary(libname);
}

void vx_os_close_library (void* handle)
{
    FreeLibrary(handle);
}

void* vx_os_get_proc_addr (void* handle, const char* procName)
{
    return GetProcAddress(handle, procName);
}

void vx_os_set_env (const char* variable, const char* value)
{
    SetEnvironmentVariable(variable, value);
}

void vx_os_get_exec_dir(char* cstr) { strcpy(cstr,_execDir); }

void vx_os_release_file_contents(char* contents)
{
    free(contents);
}

uint32_t vx_os_get_file_contents(const char* filename, char** contents)
{
    char path[VX_MAX_PATH] = { 0 };
    vx_os_get_exec_dir(path);
    strcat(strcat(path, "/"), filename);

    int size = 0;
	FILE *f = fopen(path, "rb");
	if (f == NULL) { 
		*contents = NULL;
		return 0;
	}

	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);
	*contents = (char *)calloc(size, 1);
	if (size != fread(*contents, sizeof(char), size, f)) { 
		free(*contents);
		return 0;
	} 
	fclose(f);
	return size;
}
#endif