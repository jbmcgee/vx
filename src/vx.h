#pragma once
#include <stdint.h>

#define VX_MAX_PATH 1024
#define ARRAY_SIZE(n) (sizeof(n) / sizeof(*n))
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define UNUSED(x) (void)(x)

// OS
typedef struct
{
    uint32_t width;
    uint32_t height;
    uint32_t isOpen;
    void* handle;
    void* instance;
}window_t;


window_t*       os_get_window    ();
void            os_get_exec_dir  (char*);
void*           os_open_library  (const char*);
void            os_close_library (void*);
void*           os_get_proc_addr (void*, const char*);
void            os_set_env       (const char*, const char*);
uint32_t        os_get_file_contents         (const char*, char**);
void            os_release_file_contents     (char*);

// GFX
#include <vulkan/vulkan.h>

#define VKCHECK(call)               \
do{                                 \
    VkResult result = call;         \
    assert(VK_SUCCESS ==  result);  \
} while(0)                

typedef struct  {
    VkInstance instance;
    VkPhysicalDevice physical_device;
    VkDevice device;
    uint32_t graphics_index;
    uint32_t present_index;
}device_t;

typedef struct  {
    VkImage* images;
    uint32_t width;
    uint32_t height;
    uint32_t images_count;
}swapchain_t;

typedef struct  {
    VkBuffer buffer;
    VkDeviceMemory* memory;
    void* data;
    uint64_t size;
}buffer_t;

typedef struct {
	VkImage image;
	VkImageView imageView;
	VkDeviceMemory memory;
}image_t;

typedef struct {
    VkShaderModule module;
} shader_t;

// Device
void vk_device_create(device_t* device);

// Resources
void vk_buffer_create(buffer_t* result, VkDevice device, const VkPhysicalDeviceMemoryProperties* memoryProperties, uint32_t size, VkBufferUsageFlags usage, VkMemoryPropertyFlags memoryFlags);
void vk_buffer_destroy(const buffer_t* buffer, VkDevice device);
void vk_buffer_upload(VkDevice device, VkCommandPool commandPool, VkCommandBuffer commandBuffer, VkQueue queue, const buffer_t* buffer, const buffer_t* scratch, const void* data, uint64_t size);

void vk_image_create(image_t* result, VkDevice device, const VkPhysicalDeviceMemoryProperties* memoryProperties, uint32_t width, uint32_t height, uint32_t mipLevels, VkFormat format, VkImageUsageFlags usage);
void vk_image_destroy(const image_t* image, VkDevice device);

void vk_shader_create(shader_t* shader, VkDevice device, const char* path);


// user defined
void    vx_init     (); 
void    vx_uninit   (); 
void    vx_render   (); 
void    vx_update   ();