#include "vx.h"
#include "vk.inl"

#if defined(VX_WIN32)
#define VK_USE_PLATFORM_WIN32_KHR
#define VULKAN_LIB_NAME "vulkan-1.dll"
#define ICD_FILE_NAMES ""
const char* _instance_ext[] =
{
    VK_KHR_SURFACE_EXTENSION_NAME,
    VK_KHR_WIN32_SURFACE_EXTENSION_NAME
};

#elif defined(VX_OSX)
#define VK_USE_PLATFORM_MACOS_MVK
#define VULKAN_LIB_NAME "libvulkan.1.dylib"
#define ICD_FILE_NAMES "MoltenVK_icd.json"
const char* _instance_ext[] =
{
    VK_KHR_SURFACE_EXTENSION_NAME,
    VK_MVK_MACOS_SURFACE_EXTENSION_NAME
};
#endif

const char* _device_ext[] = 
{
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
};

 void* _lib_vulkan = NULL;

void init_env()
{ 
    char path[VX_MAX_PATH] = { 0 };
    char icd[VX_MAX_PATH] = { 0 };

    os_get_exec_dir(path);
    os_get_exec_dir(icd);
    strcat(icd, "/"ICD_FILE_NAMES);

    os_set_env("VK_LAYER_PATH", path);
    #if defined(VX_OSX)
    os_set_env("VK_ICD_FILENAMES", icd);
    #endif  
}

void init_api()
{
    char path[VX_MAX_PATH] = { 0 };

    os_get_exec_dir(path);
    strcat(path, "/"VULKAN_LIB_NAME);
    _lib_vulkan = os_open_library(path);
    assert(_lib_vulkan != NULL);

#define  VK_EXPORT(n, ...) if( !(n = (PFN_##n)os_get_proc_addr( _lib_vulkan, #n )) ) assert(NULL);
    VK_EXPORTS
#undef  VK_EXPORT

#define VK_GLOBAL_IMPORT(n, ...) if( !(n = (PFN_##n)vkGetInstanceProcAddr( NULL, #n )) ) assert(NULL);
    VK_GLOBAL_IMPORTS
#undef VK_GLOBAL_IMPORT
}