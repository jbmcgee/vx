#include "vx.h"

#define VK_NO_PROTOTYPES

#if defined(VX_WIN32)
#define VK_USE_PLATFORM_WIN32_KHR
#define VULKAN_LIB_NAME "vulkan-1.dll"
#define ICD_FILE_NAMES ""
#elif defined(VX_OSX)
#define VK_USE_PLATFORM_MACOS_MVK
#define VULKAN_LIB_NAME "libvulkan.1.dylib"
#define ICD_FILE_NAMES "MoltenVK_icd.json"
#endif

#include <vulkan/vulkan.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define VK_EXPORTED_FUNCTIONS                                   \
VK_EXPORTED_FUNCTION(vkGetInstanceProcAddr)

#define VK_GLOBAL_FUNCTIONS                                     \
VK_GLOBAL_FUNCTION(vkCreateInstance)                            \
VK_GLOBAL_FUNCTION(vkEnumerateInstanceExtensionProperties)      \
VK_GLOBAL_FUNCTION(vkEnumerateInstanceLayerProperties)       

#if defined(VX_WIN32)
#define VK_INSTANCE_FUNCTION_EXTENSIONS                         \
VK_INSTANCE_FUNCTION(vkCreateWin32SurfaceKHR)
#elif defined(VX_OSX)
#define VK_INSTANCE_FUNCTION_EXTENSIONS                         \
VK_INSTANCE_FUNCTION(vkCreateMacOSSurfaceMVK) 
#endif

#define VK_INSTANCE_FUNCTIONS                                   \
VK_INSTANCE_FUNCTION(vkDestroyInstance)                         \
VK_INSTANCE_FUNCTION(vkEnumeratePhysicalDevices)                \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceProperties)             \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceFeatures)               \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceQueueFamilyProperties)  \
VK_INSTANCE_FUNCTION(vkCreateDevice)                            \
VK_INSTANCE_FUNCTION(vkGetDeviceProcAddr)                       \
VK_INSTANCE_FUNCTION(vkEnumerateDeviceExtensionProperties)      \
VK_INSTANCE_FUNCTION(vkDestroySurfaceKHR)                       \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceSurfaceSupportKHR)      \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceSurfaceCapabilitiesKHR) \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceSurfaceFormatsKHR)      \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceSurfacePresentModesKHR) \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceSurfacePresentModesKHR) \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceSurfacePresentModesKHR) \
VK_INSTANCE_FUNCTION(vkGetPhysicalDeviceMemoryProperties)       \
VK_INSTANCE_FUNCTION_EXTENSIONS


#define VK_DEVICE_FUNCTIONS                                     \
VK_DEVICE_FUNCTION(vkGetDeviceQueue)                            \
VK_DEVICE_FUNCTION(vkDestroyDevice)                             \
VK_DEVICE_FUNCTION(vkDeviceWaitIdle)                            \
VK_DEVICE_FUNCTION(vkCreateSwapchainKHR)                        \
VK_DEVICE_FUNCTION(vkDestroySwapchainKHR)                       \
VK_DEVICE_FUNCTION(vkGetSwapchainImagesKHR)                     \
VK_DEVICE_FUNCTION(vkAcquireNextImageKHR)                       \
VK_DEVICE_FUNCTION(vkQueuePresentKHR)                           \
VK_DEVICE_FUNCTION(vkQueueSubmit)                               \
VK_DEVICE_FUNCTION(vkCreateSemaphore)                           \
VK_DEVICE_FUNCTION(vkDestroySemaphore)                          \
VK_DEVICE_FUNCTION(vkCreateCommandPool)                         \
VK_DEVICE_FUNCTION(vkAllocateCommandBuffers)                    \
VK_DEVICE_FUNCTION(vkBeginCommandBuffer)                        \
VK_DEVICE_FUNCTION(vkCmdPipelineBarrier)                        \
VK_DEVICE_FUNCTION(vkCmdClearColorImage)                        \
VK_DEVICE_FUNCTION(vkEndCommandBuffer)                          \
VK_DEVICE_FUNCTION(vkDestroyCommandPool)                        \
VK_DEVICE_FUNCTION(vkFreeCommandBuffers)                        \
VK_DEVICE_FUNCTION(vkCreateFence)                               \
VK_DEVICE_FUNCTION(vkWaitForFences)                             \
VK_DEVICE_FUNCTION(vkResetFences)                               \
VK_DEVICE_FUNCTION(vkDestroyFence)                              \
VK_DEVICE_FUNCTION(vkCreateImageView)                           \
VK_DEVICE_FUNCTION(vkCreateFramebuffer)                         \
VK_DEVICE_FUNCTION(vkCreatePipelineLayout)                      \
VK_DEVICE_FUNCTION(vkCreateRenderPass)                          \
VK_DEVICE_FUNCTION(vkCreateShaderModule)                        \
VK_DEVICE_FUNCTION(vkCreateGraphicsPipelines)                   \
VK_DEVICE_FUNCTION(vkCmdBindPipeline)                           \
VK_DEVICE_FUNCTION(vkCmdDraw)                                   \
VK_DEVICE_FUNCTION(vkCmdBeginRenderPass)                        \
VK_DEVICE_FUNCTION(vkCmdEndRenderPass)                          \
VK_DEVICE_FUNCTION(vkCmdCopyBuffer)                             \
VK_DEVICE_FUNCTION(vkDestroyPipeline)                           \
VK_DEVICE_FUNCTION(vkDestroyRenderPass)                         \
VK_DEVICE_FUNCTION(vkDestroyFramebuffer)                        \
VK_DEVICE_FUNCTION(vkDestroyShaderModule)                       \
VK_DEVICE_FUNCTION(vkDestroyPipelineLayout)                     \
VK_DEVICE_FUNCTION(vkDestroyImageView)                          \
VK_DEVICE_FUNCTION(vkCreateBuffer)                              \
VK_DEVICE_FUNCTION(vkBindBufferMemory)                          \
VK_DEVICE_FUNCTION(vkMapMemory)                                 \
VK_DEVICE_FUNCTION(vkFlushMappedMemoryRanges)                   \
VK_DEVICE_FUNCTION(vkUnmapMemory)                               \
VK_DEVICE_FUNCTION(vkGetBufferMemoryRequirements)               \
VK_DEVICE_FUNCTION(vkAllocateMemory)                            \
VK_DEVICE_FUNCTION(vkCmdSetViewport)                            \
VK_DEVICE_FUNCTION(vkCmdSetScissor)                             \
VK_DEVICE_FUNCTION(vkCmdBindVertexBuffers)                      \
VK_DEVICE_FUNCTION(vkFreeMemory)                                \
VK_DEVICE_FUNCTION(vkDestroyBuffer)

#define VK_EXPORTED_FUNCTION(n, ...) PFN_##n n;
VK_EXPORTED_FUNCTIONS
#undef VK_EXPORTED_FUNCTION

#define VK_GLOBAL_FUNCTION(n, ...) PFN_##n n;
VK_GLOBAL_FUNCTIONS
#undef VK_GLOBAL_FUNCTION

#define VK_INSTANCE_FUNCTION(n, ...) PFN_##n n;
VK_INSTANCE_FUNCTIONS
#undef VK_INSTANCE_FUNCTION

#define VK_DEVICE_FUNCTION(n, ...) PFN_##n n;
VK_DEVICE_FUNCTIONS
#undef VK_DEVICE_FUNCTION

//Config
#define kConfigMinImages 2


#define VKCHECK(call)               \
do{                                 \
    VkResult result = call;         \
    assert(VK_SUCCESS ==  result);  \
} while(0)                

#define FREE(ptr) if(ptr) free(ptr); ptr = NULL
#define kMaxFrames 3

typedef struct {
    float x, y, z, w;
    float r, g, b, a;
} vertex_t;

typedef struct {
    VkBuffer handle;
    VkDeviceMemory memory;
    uint32_t size;
}buffer_t;

typedef struct {
    VkSemaphore semaphoreFilled;
    VkSemaphore semaphoreConsumed;
    VkFence fenceConsumed;
    VkCommandBuffer cmdBuffer;
    VkFramebuffer framebuffer;
} frame_t;

 void* _libVulkan;

frame_t         _frames[kMaxFrames];
buffer_t        _vertBuffer;

VkInstance      _instance;
VkDevice        _device;
uint32_t        _queueGraphicsFamilyIndex;
uint32_t        _queuePresentFamilyIndex;
uint32_t        _swapchainImageCount;
VkQueue         _queueGraphics;
VkQueue         _queuePresent;
VkSurfaceKHR    _surface;
VkPhysicalDevice _physicalDevice;
VkSwapchainKHR  _swapchain;
VkFormat        _swapchainFormat;
VkImage*        _swapchainImages;
VkCommandPool   _graphicsQueueCmdPool;
VkRenderPass    _renderPass;
VkImageView*    _swapchainImageViews;
VkPipelineLayout _pipelineLayout;
VkPipeline      _pipeline;
VkShaderModule  _vsShaderModule;
VkShaderModule  _fsShaderModule;

uint32_t _swapchainWidth;
uint32_t _swapchainHeight;
uint32_t _frameIndex;

const char* _instanceExtensions[] =
{
    VK_KHR_SURFACE_EXTENSION_NAME,
#if defined(VX_WIN32)
    VK_KHR_WIN32_SURFACE_EXTENSION_NAME
#elif defined(VX_OSX)
    VK_MVK_MACOS_SURFACE_EXTENSION_NAME
#endif
};

const char* _deviceExtensions[] = 
{
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
};


void init_env();
void init_api();
void init_extenstions();
void init_instance();
void init_instance_api();
void init_surface();
void init_device();
void init_device_api();
void init_queues();
void init_semaphores();
void init_swapchain();
void init_command_buffers();
void init_fences();
void init_passes();
void init_pipeline();
void init_image_views();
void init_vertex_buffer();
void init_frames();
void alloc(VkBuffer buffer, VkMemoryPropertyFlagBits memFlags, VkDeviceMemory* memory);
void record(VkImage image, VkImageView imageView, VkCommandBuffer cmdBuffer, VkFramebuffer* framebuffer);
void submit();
void create_buffer(VkBufferUsageFlags usage, VkMemoryPropertyFlagBits memoryProperty, uint32_t size, buffer_t* buffer);

VkShaderModule create_shader_module(const char* src);

uint32_t vx_gfx_init()
{
    init_env();
    init_api();
    init_instance();
    init_instance_api();
    init_surface();
    init_device();
    init_device_api();
    init_queues();
    init_swapchain();
    init_image_views();
    init_passes();
    init_pipeline();
    init_frames();
    init_semaphores();
    init_fences();
    init_command_buffers();
    init_vertex_buffer();

    return 1;
}

void vx_gfx_uninit()
{
    if( _device != VK_NULL_HANDLE ) {
        vkDeviceWaitIdle(_device);

        for(frame_t* frame = _frames; frame != _frames + kMaxFrames; frame++) {
            vkDestroySemaphore(_device, frame->semaphoreFilled, NULL );
            frame->semaphoreFilled = VK_NULL_HANDLE;
            vkDestroySemaphore(_device, frame->semaphoreConsumed, NULL );
            frame->semaphoreConsumed = VK_NULL_HANDLE;
            vkDestroyFence(_device, frame->fenceConsumed, NULL);
            frame->fenceConsumed = VK_NULL_HANDLE;
            vkDestroyFramebuffer(_device, frame->framebuffer, NULL );
            frame->framebuffer = VK_NULL_HANDLE;
            vkFreeCommandBuffers(_device, _graphicsQueueCmdPool, 1, &frame->cmdBuffer);
            frame->cmdBuffer = VK_NULL_HANDLE;
        }

        if(_graphicsQueueCmdPool != VK_NULL_HANDLE){
            vkDestroyCommandPool(_device, _graphicsQueueCmdPool, NULL );
            _graphicsQueueCmdPool = VK_NULL_HANDLE; 
        }

        if(_pipelineLayout != VK_NULL_HANDLE){
            vkDestroyPipelineLayout(_device, _pipelineLayout, NULL);
            _pipelineLayout = VK_NULL_HANDLE;
        }

        if(_pipeline != VK_NULL_HANDLE) {
            vkDestroyPipeline(_device, _pipeline, NULL);
            _pipeline = VK_NULL_HANDLE;
        }

        if(_renderPass != VK_NULL_HANDLE){
            vkDestroyRenderPass(_device, _renderPass, NULL );
            _renderPass = VK_NULL_HANDLE;
        }
        if( _swapchain != VK_NULL_HANDLE ) {
            vkDestroySwapchainKHR( _device, _swapchain, NULL );
            _swapchain = VK_NULL_HANDLE;
        }

        for(uint32_t i = 0; i < _swapchainImageCount; i++) {
            if(_swapchainImageViews[i] != VK_NULL_HANDLE) { 
                vkDestroyImageView(_device, _swapchainImageViews[i], NULL);
                _swapchainImages[i] = VK_NULL_HANDLE;
            }
        }
        
        if(_vertBuffer.handle != VK_NULL_HANDLE ) {
            vkDestroyBuffer(_device, _vertBuffer.handle, NULL );
            _vertBuffer.handle = VK_NULL_HANDLE;
        }

        if(_vertBuffer.memory != VK_NULL_HANDLE ) {
            vkFreeMemory(_device, _vertBuffer.memory, NULL );
            _vertBuffer.memory = VK_NULL_HANDLE;
        }

        FREE(_swapchainImageViews);

        vkDestroyDevice(_device, NULL);
    }

    if( _surface != VK_NULL_HANDLE ) {
        vkDestroySurfaceKHR( _instance, _surface, NULL );
    }

    if( _instance != VK_NULL_HANDLE ) {
        vkDestroyInstance( _instance, NULL );
    }

    if( _libVulkan ) {
        vx_os_close_library(_libVulkan);
    }
}

void init_env()
{ 
    char path[VX_MAX_PATH] = { 0 };
    char icd[VX_MAX_PATH] = { 0 };

    vx_os_get_exec_dir(path);
    vx_os_get_exec_dir(icd);
    strcat(icd, "/"ICD_FILE_NAMES);

    vx_os_set_env("VK_LAYER_PATH", path);
    #if defined(VX_OSX)
    vx_os_set_env("VK_ICD_FILENAMES", icd);
    #endif  
}

void init_api()
{
    char path[VX_MAX_PATH] = { 0 };

    vx_os_get_exec_dir(path);
    strcat(path, "/"VULKAN_LIB_NAME);
    _libVulkan = vx_os_open_library(path);
    assert(_libVulkan != NULL);

#define  VK_EXPORTED_FUNCTION(n, ...) if( !(n = (PFN_##n)vx_os_get_proc_addr( _libVulkan, #n )) ) assert(NULL);
    VK_EXPORTED_FUNCTIONS
#undef  VK_EXPORTED_FUNCTION

#define VK_GLOBAL_FUNCTION(n, ...) if( !(n = (PFN_##n)vkGetInstanceProcAddr( NULL, #n )) ) assert(NULL);
    VK_GLOBAL_FUNCTIONS
#undef VK_GLOBAL_FUNCTION
}

void init_instance()
{
    // Check for desired extensions
    uint32_t numExtensions = 0;
    VKCHECK(vkEnumerateInstanceExtensionProperties(NULL, &numExtensions, NULL));
    VkExtensionProperties* extensionProperties = (VkExtensionProperties*) calloc(numExtensions, sizeof(VkExtensionProperties));
    VKCHECK(vkEnumerateInstanceExtensionProperties(NULL, &numExtensions, extensionProperties));

    uint32_t neededNumExtensions = ARRAY_SIZE(_instanceExtensions);
    for(uint32_t i = 0; i < neededNumExtensions; ++i ) {
        for(uint32_t j = 0; j < numExtensions; j++) {
            if( strcmp(_instanceExtensions[i], extensionProperties[j].extensionName) == 0) {
                break;
            }
            // Extension not Found 
            assert(j != (numExtensions - 1)); 
        }
    }
    FREE(extensionProperties);

    VkApplicationInfo appInfo = { 
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName = "VulkanDemo",
        .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
        .pEngineName = "VX",
        .engineVersion = VK_MAKE_VERSION(1, 0, 0),
        .apiVersion = VK_API_VERSION_1_0
     };

    VkInstanceCreateInfo createInfo = { 
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo = &appInfo,
        .ppEnabledExtensionNames = _instanceExtensions,
        .enabledExtensionCount = neededNumExtensions
    };

    VKCHECK(vkCreateInstance(&createInfo, NULL, &_instance));
}

void init_instance_api()
{
#define VK_INSTANCE_FUNCTION(n, ...) if( !(n = (PFN_##n)vkGetInstanceProcAddr( _instance, #n )) ) assert(NULL);
    VK_INSTANCE_FUNCTIONS
#undef VK_INSTANCE_FUNCTION
}

void init_device()
{
    uint32_t numDevices = 0;

    VKCHECK(vkEnumeratePhysicalDevices(_instance, &numDevices, NULL));
    VkPhysicalDevice* devices = (VkPhysicalDevice*) calloc(numDevices, sizeof(VkPhysicalDevice));
    VKCHECK(vkEnumeratePhysicalDevices(_instance, &numDevices, devices));

    VkPhysicalDevice selectedDevice = { 0 };
    uint32_t selectedDeviceCompatibility = 0;
    uint32_t selectedDeviceGraphicsQueue = UINT32_MAX;
    uint32_t selectedDevicePresentQueue = UINT32_MAX;

    const uint32_t kFlagUncompatible        = 0x00;
    const uint32_t kFlagExtentions          = 0x01;
    const uint32_t kFlagFeatures            = 0x02;
    const uint32_t kFlagGraphics            = 0x04;
    const uint32_t kFlagPresent             = 0x08;
    const uint32_t kFlagGraphicsAndPresent  = 0x10;

    UNUSED(kFlagFeatures);

    // Rank Available Devices Compatibility
    for(uint32_t devIndx =0; ( devIndx < numDevices); devIndx++ ) {
        VkPhysicalDevice device = devices[devIndx];
        uint32_t deviceCompatibilty = 0;
        uint32_t queueGraphicsFamilyIndex = UINT32_MAX;
        uint32_t queuePresentFamilyIndex = UINT32_MAX;

        // Check Device Extensions
        uint32_t deviceExtensionsCount = 0;
        VKCHECK(vkEnumerateDeviceExtensionProperties(device, NULL, &deviceExtensionsCount, NULL));
        VkExtensionProperties* deviceExtensionProperties = (VkExtensionProperties*) calloc(deviceExtensionsCount, sizeof(VkExtensionProperties));
        VKCHECK(vkEnumerateDeviceExtensionProperties(device, NULL, &deviceExtensionsCount, deviceExtensionProperties));

        uint32_t neededExtensionsCount = ARRAY_SIZE(_deviceExtensions);
        for(uint32_t i = 0; i < neededExtensionsCount; ++i ) {
            for(uint32_t j = 0; j < deviceExtensionsCount; j++) {
                if( strcmp(_deviceExtensions[i], deviceExtensionProperties[j].extensionName) == 0) {
                    deviceCompatibilty |= kFlagExtentions;
                    break;
                }
                // Extension not Found 
                if(j == (deviceExtensionsCount - 1)) {
                    deviceCompatibilty = kFlagUncompatible;
                    break;
                } 
            }
        }
        FREE(deviceExtensionProperties);

        if(deviceCompatibilty == kFlagUncompatible)
            continue;

        //TODO: Filter Device by Features
        VkPhysicalDeviceProperties deviceProperties;
        VkPhysicalDeviceFeatures   deviceFeatures;
        vkGetPhysicalDeviceProperties(device, &deviceProperties );
        vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

        if(deviceCompatibilty == kFlagUncompatible)
            continue;

        // Check Device Queues
        uint32_t queueFamiliesCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties( device, &queueFamiliesCount, NULL );
        VkQueueFamilyProperties* queueFamilyProperties = (VkQueueFamilyProperties*) calloc(queueFamiliesCount, sizeof(VkQueueFamilyProperties));
        vkGetPhysicalDeviceQueueFamilyProperties( device, &queueFamiliesCount, queueFamilyProperties );

        for(uint32_t j = 0; j < queueFamiliesCount; j++) {
            VkQueueFamilyProperties* queueFamilyProperty  = &queueFamilyProperties[j];
            VkBool32 presentSupported = VK_FALSE;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, j, _surface, &presentSupported);
            if((queueFamilyProperty->queueCount > 0) && (queueFamilyProperty->queueFlags & VK_QUEUE_GRAPHICS_BIT) && 
                (presentSupported == VK_TRUE)) {
                queueGraphicsFamilyIndex = j;
                queuePresentFamilyIndex = j;
                deviceCompatibilty |= kFlagGraphicsAndPresent;
                break;
            }
            if((queueFamilyProperty->queueCount > 0) && (queueFamilyProperty->queueFlags & VK_QUEUE_GRAPHICS_BIT)) {
                if(queueGraphicsFamilyIndex == UINT32_MAX) {
                    queueGraphicsFamilyIndex = j;
                    deviceCompatibilty |= kFlagGraphics;
                }
            }

            if((queueFamilyProperty->queueCount > 0) && (presentSupported == VK_TRUE)) {
                if(queueGraphicsFamilyIndex == UINT32_MAX) {
                    queuePresentFamilyIndex = j;
                    deviceCompatibilty |= kFlagPresent;                    
                }
            }
        }
        FREE(queueFamilyProperties);

        if( ((!(deviceCompatibilty & kFlagGraphics)) || (!(deviceCompatibilty & kFlagPresent))) && 
        (!(deviceCompatibilty & kFlagGraphicsAndPresent)))
            deviceCompatibilty = 0;

        if(deviceCompatibilty > selectedDeviceCompatibility) {
            selectedDevice = device;
            selectedDeviceCompatibility = deviceCompatibilty;
            selectedDeviceGraphicsQueue = queueGraphicsFamilyIndex;
            selectedDevicePresentQueue = queuePresentFamilyIndex;
        }
    }
    FREE(devices);

    assert(selectedDeviceCompatibility != kFlagUncompatible);

    float queuePriorities[1] = { 1.0 };
    VkDeviceQueueCreateInfo pQueueCreateInfo[2];
    uint32_t queueCreateInfoCount = 0;

    VkDeviceQueueCreateInfo queueCreateInfo = { 
        .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .queueCount = sizeof(queuePriorities) / sizeof(float),
        .pQueuePriorities = queuePriorities,
        .queueFamilyIndex = selectedDeviceGraphicsQueue
    };
    pQueueCreateInfo[queueCreateInfoCount++] = queueCreateInfo;

    if(selectedDeviceGraphicsQueue != selectedDevicePresentQueue) {
        VkDeviceQueueCreateInfo queueCreateInfo = { 
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .pNext = NULL,
            .flags = 0,
            .queueCount = sizeof(queuePriorities) / sizeof(float),
            .pQueuePriorities = queuePriorities,
            .queueFamilyIndex = selectedDevicePresentQueue
        };
        pQueueCreateInfo[queueCreateInfoCount++] = queueCreateInfo;
    }

    VkDeviceCreateInfo deviceCreateInfo = {
      .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .queueCreateInfoCount = queueCreateInfoCount,
      .pQueueCreateInfos = pQueueCreateInfo,
      .enabledLayerCount = 0,
      .ppEnabledLayerNames = NULL,
      .enabledExtensionCount = ARRAY_SIZE(_deviceExtensions),
      .ppEnabledExtensionNames = _deviceExtensions,
      .pEnabledFeatures = NULL
    };

    VKCHECK(vkCreateDevice(selectedDevice, &deviceCreateInfo, NULL, &_device));

    _physicalDevice = selectedDevice;
    _queueGraphicsFamilyIndex = selectedDeviceGraphicsQueue;
    _queuePresentFamilyIndex = selectedDevicePresentQueue;
}

void uninit_instance()
{
    vkDestroyInstance(_instance, NULL);
}

void init_device_api()
{
#define VK_DEVICE_FUNCTION(n, ...) if( !(n = (PFN_##n)vkGetDeviceProcAddr( _device, #n )) ) assert(NULL);
    VK_DEVICE_FUNCTIONS
#undef VK_INSTANCE_FUNCTION
}

void init_queues()
{
    vkGetDeviceQueue(_device, _queueGraphicsFamilyIndex, 0, &_queueGraphics);
    vkGetDeviceQueue(_device, _queuePresentFamilyIndex, 0, &_queuePresent);
}

void init_surface()
{
    vx_window_t* pWindow = vx_os_get_window();

#if defined (VX_WIN32)
    VkWin32SurfaceCreateInfoKHR surfaceCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR,
        .pNext = NULL,
        .flags = 0,
        .hinstance = pWindow->instance,
        .hwnd = pWindow->handle,
    };
    VKCHECK(vkCreateWin32SurfaceKHR( _instance, &surfaceCreateInfo, NULL, &_surface ));

#elif defined(VX_OSX)
    VkMacOSSurfaceCreateInfoMVK surfaceCreateInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_MACOS_SURFACE_CREATE_INFO_MVK,
        .pNext = NULL,
        .flags = 0,
        .pView = pWindow->handle
    };
    VKCHECK(vkCreateMacOSSurfaceMVK( _instance, &surfaceCreateInfo, NULL, &_surface ));
#endif
}

void init_semaphores()
{
    VkSemaphoreCreateInfo semaphoreCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0
    };

    for(frame_t* frame = _frames; frame != _frames + kMaxFrames; frame++) {
        VKCHECK(vkCreateSemaphore(_device, &semaphoreCreateInfo, NULL, &frame->semaphoreFilled));
        VKCHECK(vkCreateSemaphore( _device, &semaphoreCreateInfo, NULL, &frame->semaphoreConsumed));
    }
}

void init_swapchain()
{
    VkSurfaceCapabilitiesKHR surfaceCapabilities;
    VKCHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(_physicalDevice, _surface, &surfaceCapabilities ));

    uint32_t minImages = kConfigMinImages;
    if(minImages < surfaceCapabilities.minImageCount)
        minImages = surfaceCapabilities.minImageCount;

    uint32_t surfaceFormatCount = 0;
    VKCHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(_physicalDevice, _surface, &surfaceFormatCount, NULL));
    VkSurfaceFormatKHR* surfaceFormats = (VkSurfaceFormatKHR*) calloc(surfaceFormatCount, sizeof(VkSurfaceFormatKHR));
    VKCHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(_physicalDevice, _surface, &surfaceFormatCount, surfaceFormats));

    uint32_t presentModeCount = 0;
    vkGetPhysicalDeviceSurfacePresentModesKHR(_physicalDevice, _surface, &presentModeCount, NULL);
    VkPresentModeKHR* presentModes = (VkPresentModeKHR*) calloc(presentModeCount, sizeof(VkPresentModeKHR));
    vkGetPhysicalDeviceSurfacePresentModesKHR(_physicalDevice, _surface, &presentModeCount, presentModes);

    // If Undefined then any format is allowed
    VkSurfaceFormatKHR selectedFormat = surfaceFormats[0];
    if((surfaceFormatCount == 1) && (surfaceFormats[0].format == VK_FORMAT_UNDEFINED) ) {
        selectedFormat.format = VK_FORMAT_R8G8B8A8_UNORM;
        selectedFormat.colorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
    }
    // else look for a format that is suitable
    else {
        for(uint32_t i = 0; i < surfaceFormatCount; i++) {
            selectedFormat = surfaceFormats[i];
            if((selectedFormat.format == VK_FORMAT_R8G8B8A8_UNORM) ||
                    (selectedFormat.format == VK_FORMAT_B8G8R8A8_UNORM)) {
                break;
            }
        }
    }

   // If this is so we define the size by ourselves but it must fit within defined confines
    VkExtent2D swapchainExtent = { 0 };
    if( surfaceCapabilities.currentExtent.width == -1 ) {
        swapchainExtent.width = 640;
        swapchainExtent.height = 480;
        if( swapchainExtent.width < surfaceCapabilities.minImageExtent.width ) {
            swapchainExtent.width = surfaceCapabilities.minImageExtent.width;
        }
        if( swapchainExtent.height < surfaceCapabilities.minImageExtent.height ) {
            swapchainExtent.height = surfaceCapabilities.minImageExtent.height;
        }
        if( swapchainExtent.width > surfaceCapabilities.maxImageExtent.width ) {
            swapchainExtent.width = surfaceCapabilities.maxImageExtent.width;
        }
        if( swapchainExtent.height > surfaceCapabilities.maxImageExtent.height ) {
            swapchainExtent.height = surfaceCapabilities.maxImageExtent.height;
        }
    }
    else {
        swapchainExtent = surfaceCapabilities.currentExtent;
    }

    // Assert the we can render to the swapchain
    assert(surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);
    VkImageUsageFlags imageUsageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    //Use no post transform unless we have to
    VkSurfaceTransformFlagBitsKHR transformFlags = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    if(surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
        transformFlags = surfaceCapabilities.currentTransform;
    }

    // Try Immediate, if not available, FIFO
    VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;
    for(uint32_t i = 0; i < presentModeCount; i++){
        if(presentModes[i] == VK_PRESENT_MODE_IMMEDIATE_KHR)
            presentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
    }

    uint32_t queueFamilyIndices[] = { _queueGraphicsFamilyIndex, _queuePresentFamilyIndex };

    VkSharingMode imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    if(_queueGraphicsFamilyIndex != _queuePresentFamilyIndex)
        imageSharingMode = VK_SHARING_MODE_CONCURRENT;

    VkSwapchainCreateInfoKHR swapchainCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .pNext = NULL,
        .flags = 0,
        .surface = _surface,
        .minImageCount = minImages,
        .imageFormat = selectedFormat.format,
        .imageColorSpace = selectedFormat.colorSpace,
        .imageExtent = swapchainExtent,
        .imageArrayLayers = 1,
        .imageUsage = imageUsageFlags,
        .imageSharingMode = imageSharingMode,
        .queueFamilyIndexCount = ARRAY_SIZE(queueFamilyIndices),
        .pQueueFamilyIndices = queueFamilyIndices,
        .preTransform = transformFlags,
        .presentMode = presentMode,
        .clipped = VK_TRUE,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .oldSwapchain = VK_NULL_HANDLE
    };  
    
    VKCHECK(vkCreateSwapchainKHR(_device, &swapchainCreateInfo, NULL, &_swapchain));
    VKCHECK(vkGetSwapchainImagesKHR(_device, _swapchain, &_swapchainImageCount, NULL));

    _swapchainImages = (VkImage*) calloc(_swapchainImageCount, sizeof(VkImage));
    VKCHECK(vkGetSwapchainImagesKHR(_device, _swapchain, &_swapchainImageCount, _swapchainImages));
    _swapchainFormat = selectedFormat.format;
    _swapchainWidth = swapchainExtent.width;
    _swapchainHeight = swapchainExtent.height;

    FREE(presentModes);
    FREE(surfaceFormats);
}

void init_command_buffers()
{
    //Initialize Command Buffers
    VkCommandPoolCreateInfo commandPoolCreateInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext = NULL,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT | VK_COMMAND_POOL_CREATE_TRANSIENT_BIT,
        .queueFamilyIndex = _queuePresentFamilyIndex
    };

    VKCHECK(vkCreateCommandPool(_device, &commandPoolCreateInfo, NULL, &_graphicsQueueCmdPool));

    VkCommandBufferAllocateInfo cmdBufferAllocateInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = NULL,
        .commandPool = _graphicsQueueCmdPool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
    };

    for(frame_t* frame = _frames; frame != _frames + kMaxFrames; frame++) {
        VKCHECK(vkAllocateCommandBuffers(_device, &cmdBufferAllocateInfo, &frame->cmdBuffer));
    }
}

void init_fences()
{
    VkFenceCreateInfo fenceCreateInfo = {
    	.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .pNext = NULL,
    	.flags = VK_FENCE_CREATE_SIGNALED_BIT
    };

    for(frame_t* frame = _frames; frame != _frames + kMaxFrames; frame++) {
        VKCHECK(vkCreateFence(_device, &fenceCreateInfo, NULL, &frame->fenceConsumed));
    }
}

void record(VkImage image, VkImageView imageView, VkCommandBuffer cmdBuffer, VkFramebuffer* framebuffer)
{
    if(*framebuffer != VK_NULL_HANDLE){
        vkDestroyFramebuffer(_device, *framebuffer, NULL );
        *framebuffer = VK_NULL_HANDLE;
    }

    VkFramebufferCreateInfo framebufferCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .renderPass = _renderPass,
        .attachmentCount = 1,
        .pAttachments = &imageView,
        .width = _swapchainWidth,
        .height = _swapchainHeight,
        .layers = 1
    };

    VKCHECK(vkCreateFramebuffer(_device, &framebufferCreateInfo, NULL, framebuffer));

    VkCommandBufferBeginInfo cmdBufferBeginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = NULL,
        .flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
        .pInheritanceInfo = NULL
    };

    const VkClearValue clearColor = {
        .color = { .float32 = { 1.0f, 0.8f, 0.4f, 0.0f } },
    };

    VkImageSubresourceRange imageSubresourceRange = {
        .aspectMask =VK_IMAGE_ASPECT_COLOR_BIT,
        .baseMipLevel = 0,
        .levelCount = 1,
        .baseArrayLayer = 0,
        .layerCount = 1
    };

    VkImageMemoryBarrier barrierPresentToRender = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = NULL,
        .srcAccessMask = VK_ACCESS_MEMORY_READ_BIT,
        .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .srcQueueFamilyIndex = _queuePresentFamilyIndex,
        .dstQueueFamilyIndex = _queueGraphicsFamilyIndex,
        .image = image,
        .subresourceRange = imageSubresourceRange
    };

    VkImageMemoryBarrier barrierRenderToPresent = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = NULL,
        .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
        .srcQueueFamilyIndex = _queueGraphicsFamilyIndex,
        .dstQueueFamilyIndex = _queuePresentFamilyIndex,
        .image = image,
        .subresourceRange = imageSubresourceRange
    };

    VKCHECK(vkBeginCommandBuffer(cmdBuffer, &cmdBufferBeginInfo));
    vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1, &barrierPresentToRender);

    VkRenderPassBeginInfo renderPassBeginInfo = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .pNext = NULL,
        .renderPass = _renderPass,
        .framebuffer = *framebuffer,
        .renderArea = {
                .offset = {.x = 0, .y= 0 },        
                .extent = {.width = _swapchainWidth, .height = _swapchainHeight }
            },
        .clearValueCount = 1,
        .pClearValues = &clearColor
    };

    vkCmdBeginRenderPass(cmdBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE );
    vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, _pipeline );

    VkViewport viewport = {
        .x = 0.0f,
        .y = 0.0f,
        .width = _swapchainWidth,
        .height = _swapchainHeight,
        .minDepth = 0.0f,
        .maxDepth = 1.0f
    };

    VkRect2D scissor = {
        .offset = { 
            .x = 0,
            .y = 0
        },
        .extent = {
            .width = _swapchainWidth,
            .height = _swapchainHeight
        }
    };
    vkCmdSetViewport(cmdBuffer, 0, 1, &viewport);
    vkCmdSetScissor(cmdBuffer, 0, 1, &scissor);

    VkDeviceSize offset = 0;
    vkCmdBindVertexBuffers( cmdBuffer, 0, 1, &_vertBuffer.handle, &offset );

    vkCmdDraw(cmdBuffer, 4, 1, 0, 0 );

    vkCmdEndRenderPass(cmdBuffer);
    vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0, NULL, 1, &barrierRenderToPresent );
    VKCHECK(vkEndCommandBuffer(cmdBuffer));
}

void submit()
{
    frame_t* frame = _frames + (_frameIndex++ % kMaxFrames );

    VKCHECK(vkWaitForFences(_device, 1, &frame->fenceConsumed, VK_TRUE, UINT64_MAX));
	VKCHECK(vkResetFences(_device, 1, &frame->fenceConsumed));

    // Get next swapchain
    uint32_t imageIndex = 0;
    VKCHECK(vkAcquireNextImageKHR(_device, _swapchain, UINT64_MAX, frame->semaphoreFilled, VK_NULL_HANDLE, &imageIndex));

    record(_swapchainImages[imageIndex], _swapchainImageViews[imageIndex], frame->cmdBuffer, &frame->framebuffer);

    VkPipelineStageFlags piplineStageFlags = VK_PIPELINE_STAGE_TRANSFER_BIT;
    VkSubmitInfo submitInfo = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext = NULL,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &frame->semaphoreFilled,
        .pWaitDstStageMask = &piplineStageFlags,
        .commandBufferCount = 1,
        .pCommandBuffers = &frame->cmdBuffer,
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = &frame->semaphoreConsumed
    };

    VKCHECK(vkQueueSubmit( _queuePresent, 1, &submitInfo, frame->fenceConsumed));

    VkPresentInfoKHR presentInfo = {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .pNext = NULL,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &frame->semaphoreConsumed,
        .swapchainCount = 1,
        .pSwapchains = &_swapchain,
        .pImageIndices = &imageIndex,
        .pResults = NULL
    };

    VKCHECK(vkQueuePresentKHR( _queuePresent, &presentInfo ));
}

void init_passes()
{
    VkAttachmentDescription attachmentDescs[] =
    {
        {
            .flags = 0,
            .format = _swapchainFormat,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
        }
    };

    VkAttachmentReference colorAttachmentRefs[] = 
    {
        {
            .attachment = 0,
            .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        }
    };

    VkSubpassDescription subpassDescs[] =
    {
        {
            .flags = 0,
            .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
            .inputAttachmentCount = 0,
            .pInputAttachments = NULL,
            .colorAttachmentCount = 1,
            .pColorAttachments = colorAttachmentRefs,
            .pResolveAttachments = NULL,
            .pDepthStencilAttachment = NULL,
            .preserveAttachmentCount = 0,
            .pPreserveAttachments = NULL
        }
    };
    
    VkSubpassDependency subpassDeps[] = {
        {
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .srcAccessMask = VK_ACCESS_MEMORY_READ_BIT,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            .dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
        },
        {
            .srcSubpass = 0,
            .dstSubpass = VK_SUBPASS_EXTERNAL,
            .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
            .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
            .dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
        }
    };

    VkRenderPassCreateInfo renderPassCreateInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .attachmentCount = 1,
        .pAttachments = attachmentDescs,
        .subpassCount = 1,
        .pSubpasses = subpassDescs,
        .dependencyCount = sizeof(subpassDeps) / sizeof(VkSubpassDependency),
        .pDependencies = subpassDeps
    };

    VKCHECK(vkCreateRenderPass( _device, &renderPassCreateInfo, NULL, &_renderPass));
}

void init_image_views()
{
    _swapchainImageViews = (VkImageView*) calloc(_swapchainImageCount, sizeof(VkImageView));

    for( uint32_t i = 0; i < _swapchainImageCount; ++i ) {
        VkImageViewCreateInfo imageViewCreateInfo =
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .pNext = NULL,
            .flags = 0,
            .image = _swapchainImages[i],
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = _swapchainFormat,
            .components = 
            {
                .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                .a = VK_COMPONENT_SWIZZLE_IDENTITY
            },
            .subresourceRange =
            {
                . aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1
            }
        };
        VKCHECK(vkCreateImageView( _device, &imageViewCreateInfo, NULL, &_swapchainImageViews[i]));
    }
}

VkShaderModule create_shader_module(const char* src)
{
    char* buffer;
    uint32_t bufferSize = vx_os_get_file_contents(src, &buffer);

    VkShaderModuleCreateInfo shaderModuleCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .codeSize = bufferSize,
        .pCode = (uint32_t*)buffer
    };

    VkShaderModule shaderModule;
    VKCHECK(vkCreateShaderModule( _device, &shaderModuleCreateInfo, NULL, &shaderModule));

    vx_os_release_file_contents(buffer);

    return shaderModule;
}

void init_pipeline()
{
    _vsShaderModule = create_shader_module("shaders/main.vert.spirv");
    _fsShaderModule = create_shader_module("shaders/main.frag.spirv");

    VkPipelineShaderStageCreateInfo shaderStageCreateInfos[] =
    {
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext = NULL,
            .stage = VK_SHADER_STAGE_VERTEX_BIT,
            .flags = 0,
            .module = _vsShaderModule,
            .pName = "main",
            .pSpecializationInfo = NULL
        },
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext = NULL,
            .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
            .flags = 0,
            .module = _fsShaderModule,
            .pName = "main",
            .pSpecializationInfo = NULL       
        }
    };

    VkVertexInputBindingDescription vertexInputBindingDescs[] = {
        {
            .binding = 0,
            .stride = sizeof(vertex_t),
            .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
        }
    };

    VkVertexInputAttributeDescription vertexInputAttributeDescs[] =  {
        {
            .location = 0,
            .binding = 0,
            .format = VK_FORMAT_R32G32B32A32_SFLOAT,
            .offset = offsetof(vertex_t, x)
        },
        {
            .location = 1,
            .binding = 0,
            .format = VK_FORMAT_R32G32B32A32_SFLOAT,
            .offset = offsetof(vertex_t, r)
        }
    };

    VkPipelineVertexInputStateCreateInfo vertexInputStateCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .pNext = NULL, 
        .flags = 0,
        .vertexBindingDescriptionCount = sizeof(vertexInputBindingDescs) / sizeof(VkVertexInputBindingDescription),
        .pVertexBindingDescriptions = vertexInputBindingDescs,
        .vertexAttributeDescriptionCount = sizeof(vertexInputAttributeDescs) / sizeof(VkVertexInputAttributeDescription),
        .pVertexAttributeDescriptions = vertexInputAttributeDescs
    };

    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, 
        .pNext = NULL,
        .flags = 0,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,
        .primitiveRestartEnable = VK_FALSE
    };

#if 0
    VkViewport viewport = {
        .x = 0.0f,
        .y = 0.0f,
        .width = _swapchainWidth,
        .height = _swapchainHeight,
        .minDepth = 0.0f,
        .maxDepth = 1.0f
    };

    VkRect2D scissor = {
        .offset = { 
            .x = 0,
            .y = 0
        },
        .extent = {
            .width = _swapchainWidth,
            .height = _swapchainHeight
        }
    };
#endif

    // Dynamic Settings
    VkPipelineViewportStateCreateInfo viewportStateCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .viewportCount = 1,
        .pViewports = NULL,
        .scissorCount = 1,
        .pScissors = NULL
    };

    VkPipelineRasterizationStateCreateInfo rasterizationStateCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0.0f,
        .depthBiasClamp = 0.0f,
        .depthBiasSlopeFactor = 0.0f,
        .lineWidth = 1.0f
    };

    VkPipelineMultisampleStateCreateInfo multisampleStateCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .rasterizationSamples =VK_SAMPLE_COUNT_1_BIT,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 1.0f,
        .pSampleMask = NULL,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE
    };

    VkPipelineColorBlendAttachmentState colorBlendAttachmentState = {
        .blendEnable = VK_FALSE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp = VK_BLEND_OP_ADD,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
    };

    VkPipelineColorBlendStateCreateInfo colorBlendStateCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &colorBlendAttachmentState,
        .blendConstants = { 0.0f, 0.0f, 0.0f, 0.0f }
    };

    VkDynamicState dynamicStates[] = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };

    VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .dynamicStateCount = sizeof(dynamicStates) / sizeof(VkDynamicState),
        .pDynamicStates = dynamicStates
    };

    VkPipelineLayoutCreateInfo layoutCreateInfo = {
        .sType =  VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .setLayoutCount = 0,
        .pSetLayouts = NULL,
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = NULL
    };

    VKCHECK(vkCreatePipelineLayout( _device, &layoutCreateInfo, NULL, &_pipelineLayout ));

    VkGraphicsPipelineCreateInfo pipelineCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .stageCount = sizeof(shaderStageCreateInfos) / sizeof(VkPipelineShaderStageCreateInfo),
        .pStages = shaderStageCreateInfos,
        .pVertexInputState = &vertexInputStateCreateInfo,
        .pInputAssemblyState = &inputAssemblyStateCreateInfo,
        .pTessellationState = NULL,
        .pViewportState = &viewportStateCreateInfo,
        .pRasterizationState = &rasterizationStateCreateInfo,
        .pMultisampleState = &multisampleStateCreateInfo,
        .pDepthStencilState = NULL,
        .pColorBlendState = &colorBlendStateCreateInfo,
        .pDynamicState = &dynamicStateCreateInfo,
        .layout = _pipelineLayout,
        .renderPass = _renderPass,
        .subpass = 0,
        .basePipelineHandle = VK_NULL_HANDLE,
        .basePipelineIndex = -1
    };

    VKCHECK(vkCreateGraphicsPipelines(_device, VK_NULL_HANDLE, 1, &pipelineCreateInfo, NULL, &_pipeline));
}

void init_vertex_buffer() {
    vertex_t vertices[] = {
        {
            -0.7f, -0.7f, 0.0f, 1.0f,
            1.0f, 0.0f, 0.0f, 0.0f
        },
        {
            -0.7f, 0.7f, 0.0f, 1.0f,
            0.0f, 1.0f, 0.0f, 0.0f
        },
        {
            0.7f, -0.7f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f
        },
        {
            0.7f, 0.7f, 0.0f, 1.0f,
            0.3f, 0.3f, 0.3f, 0.0f
        }
    };

    uint32_t size = sizeof(vertices);

    buffer_t stagingBuffer;
    create_buffer(VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, size, &_vertBuffer);
    create_buffer(VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, size, &stagingBuffer);
 
    void *stagingData;
    VKCHECK(vkMapMemory( _device, stagingBuffer.memory, 0, stagingBuffer.size, 0, &stagingData));
    memcpy(stagingData, vertices, stagingBuffer.size);

    VkMappedMemoryRange memoryRange = {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .pNext = NULL,
        .memory = stagingBuffer.memory,
        .offset = 0,
        .size = VK_WHOLE_SIZE
    };
    vkFlushMappedMemoryRanges( _device, 1, &memoryRange );
    vkUnmapMemory( _device, stagingBuffer.memory);

    //Copy

    VkCommandBufferBeginInfo cmdBufferBeginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = NULL,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = NULL
    };

    VkBufferMemoryBarrier bufferMemBarrier = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = NULL,
        .srcAccessMask = VK_ACCESS_MEMORY_READ_BIT,
        .dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
        .srcQueueFamilyIndex = _queuePresentFamilyIndex,
        .dstQueueFamilyIndex = _queueGraphicsFamilyIndex,
        .buffer = _vertBuffer.handle,
        .offset = 0,
        .size = VK_WHOLE_SIZE
    };

    VkCommandBuffer cmdBuffer = _frames->cmdBuffer;

    VkSubmitInfo submitInfo = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext = NULL,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = NULL,
        .pWaitDstStageMask = NULL,
        .commandBufferCount = 1,
        .pCommandBuffers = &cmdBuffer,
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = NULL
    };
    
    VkBufferCopy bufferCopyInfo = {
        .srcOffset = 0,
        .dstOffset = 0,
        .size = _vertBuffer.size
    };
    VKCHECK(vkBeginCommandBuffer(cmdBuffer, &cmdBufferBeginInfo));
    vkCmdCopyBuffer(cmdBuffer, stagingBuffer.handle, _vertBuffer.handle, 1, &bufferCopyInfo);
    vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, 0, 0, NULL, 1, &bufferMemBarrier, 0, NULL);
    vkEndCommandBuffer(cmdBuffer);
    VKCHECK(vkQueueSubmit( _queueGraphics, 1, &submitInfo, VK_NULL_HANDLE));

    vkDeviceWaitIdle(_device);

    vkDestroyBuffer(_device, stagingBuffer.handle, NULL);
    vkFreeMemory(_device, stagingBuffer.memory, NULL);
}

void alloc(VkBuffer buffer, VkMemoryPropertyFlagBits flags, VkDeviceMemory* memory) {
    VkMemoryRequirements memoryRequirements;
    vkGetBufferMemoryRequirements( _device, buffer, &memoryRequirements );

    VkPhysicalDeviceMemoryProperties memoryProperties;
    vkGetPhysicalDeviceMemoryProperties( _physicalDevice, &memoryProperties );

    for( uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i ) {
        if( (memoryRequirements.memoryTypeBits & (1 << i)) &&
            (memoryProperties.memoryTypes[i].propertyFlags & flags) ) {

            VkMemoryAllocateInfo memoryAllocateInfo = {
                .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
                .pNext = NULL,
                .allocationSize = memoryRequirements.size,
                .memoryTypeIndex = i
            };

            VKCHECK(vkAllocateMemory(_device, &memoryAllocateInfo, NULL, memory));
            break;
        }
    }
}

void init_frames(){
    for(frame_t* frame = _frames; frame != _frames + kMaxFrames; frame++){
        memset(frame, 0, sizeof(frame_t));
    }
}

void create_buffer(VkBufferUsageFlags usageFlags, VkMemoryPropertyFlagBits memFlags, uint32_t size, buffer_t* buffer){
    VkBufferCreateInfo bufferCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .size = size,
        .usage = usageFlags, 
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = NULL
    };

    VKCHECK(vkCreateBuffer(_device, &bufferCreateInfo, NULL, &buffer->handle));

    alloc(buffer->handle, memFlags, &buffer->memory);

    VKCHECK(vkBindBufferMemory(_device, buffer->handle, buffer->memory, 0));

    buffer->size = size;
}



void vx_gfx_end()
{
    submit();
}

void vx_gfx_begin()
{

}