#include "vx.h"
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <QuartzCore/CAMetalLayer.h>
#include <dlfcn.h>

vx_window_t _osWindow;
char _execDir[VX_MAX_PATH];

vx_window_t* vx_os_get_window() { return &_osWindow; };
void vx_os_get_exec_dir(char* cstr) { strcpy(cstr,_execDir); }

@interface AppDelegate : NSObject<NSApplicationDelegate>
-(NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender;
@end

@implementation AppDelegate
-(NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
	_osWindow.isOpen = false;
	return NSTerminateCancel;
}
-(void)applicationWillFinishLaunching:(NSNotification *)aNotification
{
    [NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];
}
-(void)applicationDidFinishLaunching:(NSNotification *)notification
{
    [NSApp activateIgnoringOtherApps:YES];
}

@end

static CVReturn DisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*);

@interface WindowView : NSView <NSWindowDelegate>{
    CVDisplayLinkRef _displayLink;
}
@end

@implementation WindowView
- (id) initWithFrame: (NSRect) frame {
	self = [super initWithFrame:frame];

    CVDisplayLinkCreateWithActiveCGDisplays(&_displayLink);
    CVDisplayLinkSetOutputCallback(_displayLink, &DisplayLinkCallback, NULL);
    CVDisplayLinkStart(_displayLink);

    return self;
}
- (CVReturn) postFrame:(const CVTimeStamp*)time {
	return kCVReturnSuccess;
}
-(void)windowWillClose:(NSNotification*)notification
{
    _osWindow.isOpen = false;
	CVDisplayLinkStop(_displayLink);
	CVDisplayLinkRelease(_displayLink);
}

/** Indicates that the view wants to draw using the backing layer instead of using drawRect:.  */
-(BOOL) wantsUpdateLayer { return YES; }

/** Returns a Metal-compatible layer. */
+(Class) layerClass { return [CAMetalLayer class]; }

/** If the wantsLayer property is set to YES, this method will be invoked to return a layer instance. */
-(CALayer*) makeBackingLayer {
    CALayer* layer = [self.class.layerClass layer];
    CGSize viewScale = [self convertSizeToBacking: CGSizeMake(1.0, 1.0)];
    layer.contentsScale = MIN(viewScale.width, viewScale.height);
    return layer;
}
@end

static CVReturn DisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* now, const CVTimeStamp* time, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* view) {
	CVReturn result = [(WindowView*)view postFrame:time];
	return result;
}

static void set_exec_path(const char* path)
{
    strcpy(_execDir, path);
    int i = strlen(_execDir) - 1;
    while(_execDir[i] != '/') i--;
    _execDir[i] = 0;
}

int main(int argc, const char * argv[])  { 
    // Set Path
    set_exec_path(argv[0]);

    // App
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init]; 
	[NSApplication sharedApplication]; 
    AppDelegate * appDelegate = [[[AppDelegate alloc] init] autorelease]; // needs to be on a separate line
	[NSApp setDelegate:appDelegate];
	[NSApp finishLaunching];
    
	// Window
	id appName = [[NSProcessInfo processInfo] processName];
	NSUInteger windowStyle = NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable;
	NSRect windowRect = NSMakeRect(0, 0, 640, 480); 
 
	NSWindow * window = [[NSWindow alloc] initWithContentRect:windowRect 
						styleMask:windowStyle 
						backing:NSBackingStoreBuffered 
						defer:NO] ; 
    WindowView * view = [[[WindowView alloc] init] autorelease];
    [view setLayer:[CAMetalLayer layer]];
	[window setDelegate:view];
    [window setContentView:view];
    [window setTitle:appName];
	[window orderFrontRegardless]; 
    [window makeKeyAndOrderFront:window];
	[window setAcceptsMouseMovedEvents:YES];
	[window setBackgroundColor:[NSColor blackColor]];
    _osWindow.handle = view;

    //Menu
	id menubar = [[NSMenu new] autorelease];
	id appMenuItem = [[NSMenuItem new] autorelease];
	[menubar addItem:appMenuItem];
	[NSApp setMainMenu:menubar];
	id appMenu = [[NSMenu new] autorelease];
	id quitTitle = [@"Quit " stringByAppendingString:appName];
	id quitMenuItem = [[[NSMenuItem alloc] initWithTitle:quitTitle
		action:@selector(terminate:) keyEquivalent:@"q"] autorelease];
	[appMenu addItem:quitMenuItem];
	[appMenuItem setSubmenu:appMenu];

    vx_init(); 

    // Main Loop
    _osWindow.isOpen = true;
    do {
        for (;;)
        {
            NSEvent *event =
            [NSApp
                nextEventMatchingMask:NSAnyEventMask
                untilDate:[NSDate distantPast]
                inMode:NSDefaultRunLoopMode
                dequeue:YES];

            if (event == nil)
                break;

            [NSApp sendEvent:event];            
        }
        [pool drain];
        pool = [[NSAutoreleasePool alloc] init];
        
        vx_update(); 
        vx_render(); 

        [NSApp updateWindows];
    } while (_osWindow.isOpen);

    vx_uninit(); 

	[pool drain];  
	return (0); 
}

void* vx_os_open_library(const char* path)
{
    return dlopen(path, RTLD_NOW);
}

void vx_os_close_library(void* lib)
{

}

void* vx_os_get_proc_addr(void* lib, const char* proc)
{
    return dlsym(lib, proc);
}

void vx_os_set_env(const char* variable, const char* value)
{
    setenv(variable, value, 0);
}

void vx_os_release_file_contents(char* contents)
{
    free(contents);
}

uint32_t vx_os_get_file_contents(const char* filename, char** contents)
{
    char path[VX_MAX_PATH] = { 0 };
    vx_os_get_exec_dir(path);
    strcat(strcat(path, "/"), filename);

    int size = 0;
	FILE *f = fopen(path, "rb");
	if (f == NULL) { 
		*contents = NULL;
		return 0;
	}

	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);
	*contents = (char *)calloc(size, 1);
	if (size != fread(*contents, sizeof(char), size, f)) { 
		free(*contents);
		return 0;
	} 
	fclose(f);
	return size;
}