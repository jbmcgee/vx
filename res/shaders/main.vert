#version 450

layout(location = 0) in vec4 i_position;
layout(location = 1) in vec4 i_color;

out gl_PerVertex
{
  vec4 gl_Position;
};

layout(location = 0) out vec4 v_color;

void main() {
    gl_Position = i_position;
    v_color = i_color;
}
