if not defined DevEnvDir (
	call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
)

if not exist .runtime mkdir .runtime
if not exist .runtime\shaders mkdir .runtime\shaders
if not exist .build mkdir .build

set cc=cl
set cl=/link
set exe=/out:.runtime\vx.exe
set cflags= /Zi /Iext\vulkan\include /DVX_WIN32 /Fo.build\ 
set lflags= /DEBUG /PDB:.runtime\ user32.lib shell32.lib

for /R res\shaders %%f in (*) do (
	call tools\win\glslangValidator.exe -V -o .runtime\shaders\%%~nxf.spirv %%~ff
)

set src=
for /R .\src %%f in (*.c) do call set src=%%src%% %%f
%cc% %cflags% %src% %cl% %lflags% %exe%

ROBOCOPY ext\vulkan\lib\win .runtime