CC:=clang
CFLAGS:=-c -g -Wall -I./ext/vulkan/include -D VX_OSX
LDFLAGS= -framework Cocoa  -framework Quartz
SOURCEDIR:=src
BUILDDIR:=.build
RUNTIMEDIR:=.runtime
RESDIR:= res
SHADERDIR:= $(RESDIR)/shaders
SHADERBINDIR:= $(RUNTIMEDIR)/shaders
SHADEROBJS:= $(patsubst $(SHADERDIR)/%, $(SHADERBINDIR)/%.spirv, $(wildcard $(SHADERDIR)/*))

OBJECTS:=$(patsubst $(SOURCEDIR)/%.c,$(BUILDDIR)/%.o,$(wildcard $(SOURCEDIR)/*.c))
OBJECTS+=$(patsubst $(SOURCEDIR)/%.m,$(BUILDDIR)/%.o,$(wildcard $(SOURCEDIR)/*.m))

EXECUTABLE:=$(RUNTIMEDIR)/vx

all: prebuild shaders exec

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

$(BUILDDIR)/%.o: $(SOURCEDIR)/%.c
	$(CC) $(CFLAGS) $< -o $@

$(BUILDDIR)/%.o: $(SOURCEDIR)/%.m
	$(CC) $(CFLAGS) $< -o $@	

.PHONY : clean prebuild
prebuild:
	mkdir -p $(BUILDDIR)
	mkdir -p $(RUNTIMEDIR)
	mkdir -p $(SHADERBINDIR)
	cp ./ext/vulkan/lib/osx/* $(RUNTIMEDIR)
	cp ./ext/vulkan/etc/osx/* $(RUNTIMEDIR)

shaders: prebuild $(SHADEROBJS)

$(SHADERBINDIR)/%.spirv: $(SHADERDIR)/%
	./tools/osx/glslangValidator -V -o $@ $<
exec: $(EXECUTABLE)
clean :
	-rm -rf $(BUILDDIR)
	-rm -rf $(RUNTIMEDIR)